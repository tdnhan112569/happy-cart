import { Localization } from 'expo-localization';
import i18n from 'i18n-js'; 
import en from './locales/en';
import vi from './locales/vi';

i18n.fallbacks = true;
i18n.locale = Localization.locale;
i18n.translations = {
  en,
  vi,
};

export default i18n;