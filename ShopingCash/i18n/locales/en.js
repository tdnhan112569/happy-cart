export default {  
    greeting: 'Hello Guy',
    lableIncrease: 'Plus 1',
    lableDecrease: 'Sub 1',
    tab1Name:'Home',
    tab2Name:'Product',
    tab3Name:'History',
    tab4Name:'Infomation',
    lableCategory:'Categorys',
    lableIntroduce:'Introduce',
    lableContact:'Contact',
    lableBanner:'Sale off',
    headerHome:'Home',
    headerCart:'Cart',
    headerCheckout:'Checkout Information',
    contactTitle:'Comfirm',
    contactMessage:'Do you want to call now?',
    cancelTitle:'Let it latter',
    okTitle:'OK!',
    lableButtonCall:'Call OneNoTwo Shop Now!',
    placeholderSearch:'Product name',
    buttonTitleViewNow:'VIEW NOW',
    resultNotFoundTitle:'Result isn\'t found',
    productDetailTitle:'Product Detail',
    buttonBackTitle:'Back',
    cartButtonTitle:'Cart',
    lableDetailInformation:'Detail Information',
    lableChoiceSize:'Select the size',
    lableName:'Name',
    lableCategoryCheckout:'Category',
    lableDescription:'Description',
    lableInformation:'Notice',
    lableResultBuyingSuccessful:'You ordered successful!',
    lableButtonGoToCart:'Go to Cart',
    lableBuyMore:'Buy more',
    messageCartEmpty:'Your cart is empty!',
    lableDetailPackage:'Detail Package',
    lablePurchasedate:'Purchase date',
    lablePayment:'Payment',
    lableUsername:'Username',
    lablePackageCode:'Package code',
    lableTotal:'Total:',
    lableButtonBuyIt:'Buy It',
    lableConfirm:'Comfirm',
    lableMessageDialogBuy:'Please press \'Continute\' to buy now!',
    lableContinute:'Continute',
    lableStatus:'Status',
    lableThank:'Thank you for buying at OneNoTwo Shop, your package will be delivered as soon as possible!',
    lableCheckPackage:'Check package',
    lableHistoryOrderEmpty:'Don\'t have any the ordered package',
    lableOrderDetailScreen:'Order Detail',
    lableFilterBy:'Filter by',
    lablePrice:'Price:',
    lableFromTo:'From To',
    lableDiscountPercent:'Discount %',
    lableFrom:'From:',
    lableTo:'To:',
    lableDiscount:'Discount:',
    lableCancel:'Cancel',
    lableOK:'OK',
    messageAlertOver:'Discount: Value can\'t be over 100%',
    lableTitleDeleteItem:'Comfirm',
    lableMessageDeleteItem:'Press \'OK\' to delete item cart',
    lableTitleAgreeDelteItem:'OK'
  };
  