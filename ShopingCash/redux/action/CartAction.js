export default ActionCartCreator = {
    ACTION_ADD_CART: {
        type: 'ACTION-ADD-CART',
        payload: {
            'id': '',
            'name': '',
            'image': '',
            'thumbnail': '',
            'shortDescription': '',
            'categoryId': '',
            'salePrice': 0,
            'originalPrice': 0,
            'images': [],
            'thumbnails': [],
            'totalPrice': 0,
            'count': 0,
            'status': '',
            'size': ''
        }
    },
    ACTION_CHECKOUT_CART: {
        type: 'ACTION-CHECKOUT-CART',
        payload: {
            packageId: '',
            nameUser: '',
            payment: '',
            buyingDate: '',
            status:'',
            listItem: []
        }
    },
    ACTION_DELETE_ITEM_CART: {
        type: 'ACTION-DELETE-ITEM',
        payload: {
            'id': '',
            'name': '',
            'image': '',
            'thumbnail': '',
            'shortDescription': '',
            'categoryId': '',
            'salePrice': 0,
            'originalPrice': 0,
            'images': [],
            'thumbnails': [],
            'totalPrice': 0,
            'count': 0,
            'status': '',
            'size': ''
        }
    },
}