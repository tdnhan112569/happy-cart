export default ActionCreator = {
    ACTION_CREATE: {
        type:'ACTION-CREATE',
    },
    ACTION_FILTER: {
        type:'ACTION-FILTER',
        payload:{
            isFilter: false,
            withFromTo:false,
            withDiscount:false,
            withCategory:false,
            dataFromTo:{
                from:0, to:0,
            },
            discount: 0,
            categoryKey:'none'
        }
    }
}