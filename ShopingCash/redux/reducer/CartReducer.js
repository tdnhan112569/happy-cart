import AppState from '../AppState'
const CartReducer = (state = AppState , action) => {
    const payload = action.payload
    const {cartList, historyOrderList} = state
    switch(action.type) {
        case 'ACTION-ADD-CART':
            console.log('Before do it')
            console.log(cartList)
            console.log(payload)
            console.log('------------------------------------')
            if(cartList.length > 0) {
               
                let arrayFilter = cartList.filter(item=> {
                    if(item.id === payload.id && item.size === payload.size) {
                        console.log(item.size + '  ' + payload.size)
                        return item
                    }
                })
                console.log('filter')
                console.log(arrayFilter)
                if(arrayFilter.length > 0) {
                    for(let i = 0; cartList.length; i++) {
                        if(cartList[i].id === payload.id && cartList[i].size === payload.size ) {
                            cartList[i].count += payload.count
                            cartList[i].totalPrice += payload.totalPrice
                            break
                        }
                    }
                } else {
                    cartList.push(payload)
                }   
            } else {
                cartList.push(payload)
            }

            console.log('List order')
            console.log(cartList)
            return {
                ...state
            }

        case 'ACTION-CHECKOUT-CART':
             console.log('LIST ORDER ITEM FOR ORDER SCREEN')
             console.log(historyOrderList)
             let newArray = historyOrderList.slice();
             newArray.push(payload)
            return{
                ...state,
                 historyOrderList : newArray,
                 cartList: []
            }
        
        case 'ACTION-DELETE-ITEM':
            let cartListCopy = cartList.slice()
            for(let i = 0; cartListCopy.length; i++) {
                if(cartListCopy[i] === payload) {
                    cartListCopy.splice(i, 1);
                    break
                }
            }
            return {
                ...state,
                cartList:cartListCopy
            }

    }

    return state
}

export default CartReducer