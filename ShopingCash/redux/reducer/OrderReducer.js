const OrderReducer = (state , action) => {
    let value = state.counter
    switch(action.type) {
        
        case 'ACTION-INCREASE':
            const {valueIncrease} = action.payload
            value += valueIncrease
            return {
                ...state,
                counter: value
            }
        case 'ACTION-DECREASE':
            const {valueDecrease} = action.payload
            value -= valueDecrease
            return{
                ...state,
                counter: value
            }
    }

    return state
}

export default OrderReducer