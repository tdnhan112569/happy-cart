import React from 'react';
import { Text, View, TouchableOpacity, Image, Dimensions, Platform } from 'react-native';
let { height, width } = Dimensions.get('screen')

export default class ItemCart extends React.Component {

    // static navigationOptions = ({ navigation, tintColor }) => ({

    //     title: i18n.t('headerCart'),

    // })

    onPriceDisplay = (text) => {
        if ((text + '').length <= 3) return text + ''
        let i = 0
        let price = (text + '').split('')
        var priceToText = "";
        let count = 0
        for (i = price.length - 1; i >= 0; i--) {
            if (count === 3) {
                priceToText = price[i] + '.' + priceToText;
                count = 0;
            } else {
                priceToText = price[i] + priceToText
            }
            count++
        }
        return priceToText
    }

    render() {

        const { item, index } = this.props
        const { image, count, size, totalPrice, name } = item

        return (
            <View style={{
                height: 70, width: width * 0.98, flexDirection: 'row', justifyContent: 'center',
                alignItems: 'center', marginBottom: 10, elevation: 1, borderRadius: (Platform.OS === 'android') ? 2.5 : 10 , borderColor: (Platform.OS === 'ios') ? 'lightgray' : 'white',
                marginLeft: 5, marginRight: 5, borderWidth: (Platform.OS === 'ios') ? 1 : 0,
            }}>
                <View style={{ padding: 5 }}>
                    <Image style={{ height: 60, width: 60 }}
                        source={{ uri: image }}
                    />
                </View>
                <View style={{ flex: 6, flexDirection: 'column', justifyContent: 'center' }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Text>{name}</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text>Count: </Text>
                        <Text>{count}</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text>Size: </Text>
                        <Text>{size}</Text>
                    </View>
                </View>
                <View style={{ flex: 3, justifyContent: 'center', alignItems: 'center', backgroundColor: 'red', marginRight: 10, borderRadius: 10 }}>
                    <Text style={{ color: 'white', fontSize: (Platform.OS === 'android') ? 20 : 15, margin: 10 }}>{this.onPriceDisplay(totalPrice)}$</Text>
                </View>
            </View>
        );
    }
}

