import React from 'react';
import { Text, TouchableOpacity, View, Alert, TextInput, Platform } from 'react-native';
import Modal from 'react-native-modal'
import { CheckBox } from 'react-native-elements'
import { Dropdown } from 'react-native-material-dropdown';
import Category from '../../API/fakeData/CategoryData'
import { connect } from 'react-redux'
import ActionCreator from '../../redux/action/ProductAction'
import i18n from '../../i18n/i18n'

let previousPrice = {
    priceFrom: 0,
    priceTo: 0,
}

let preDiscount = {
    discount: 0
}

let arrayItem = []

class FilterModal extends React.Component {

    constructor(props) {
        super(props)
        const { filterStatus } = this.props
        const { withFromTo, withDiscount, discount, categoryKey, dataFromTo } = filterStatus
        let fromDistance = 0, toDistance = 0
        if(dataFromTo != undefined) {
            fromDistance = dataFromTo.from
            toDistance = dataFromTo.to
        }
        console.log('From to ne')
        console.log(fromDistance + ' -> ' + toDistance)
        this.state = {
            modalVisible: true,
            priceFrom: fromDistance,
            priceTo: toDistance,
            checkByDistant: (withFromTo != undefined) ? withFromTo : false,
            checkByDiscount: (withDiscount != undefined) ? withDiscount : false,
            discount: (discount != undefined) ? discount : 0,
            selectionCategory: (categoryKey != undefined) ? categoryKey : 'none'
        };

        let itemSelection = Category.body.map((item) => {
            return { value: item.name }
        })

        arrayItem = [{ value: 'none' }, ...itemSelection]

    }

    onPriceChange = (key, price) => {
        let arrayChar = (price + '').split('.' || '$')
        price = ''
        arrayChar.forEach((item) => {
            price += item
        })
        console.log('price ' + price)
        let _price = parseInt(price)
        console.log(_price)
        if (isNaN(_price)) _price = 0

        if (_price > 999999999999) {
            (key === 1) ? _price = previousPrice.priceFrom : _price = previousPrice.priceTo
        }

        const { priceFrom, priceTo } = this.state
        let priceChange = {}
        let _priceFrom = parseInt(priceFrom)
        let _priceTo = parseInt(priceTo)

        if (key === 1) {
            (_price > _priceTo) ? priceChange = { priceFrom: _price, priceTo: _price } : priceChange = { priceFrom: _price }
        } else if (key === 2) {
            (_priceFrom > _price) ? priceChange = { priceFrom: _price, priceTo: _price } : priceChange = { priceTo: _price }
        }
        previousPrice = priceChange
        console.log(priceChange)
        this.setState(priceChange)
    }

    onPriceDisplay = (text) => {
        (text + '')
        if ((text + '').length <= 3) return text + ''

        let i = 0
        let price = (text + '').split('')
        var priceToText = "";
        let count = 0
        console.log(' xu ly hien thi ' + price)
        for (i = price.length - 1; i >= 0; i--) {
            console.log(price[i])
            if (count === 3) {
                priceToText = price[i] + '.' + priceToText;
                count = 0;
            } else {
                priceToText = price[i] + priceToText
            }
            count++
        }
        console.log(' tao ne ' + priceToText)
        return priceToText
    }

    onDiscountChange = (discount) => {
        let _discount = parseInt(discount)
        if (isNaN(_discount)) _discount = 0
        let discountChange = {}
        if (_discount <= 100) {
            discountChange = { discount: _discount }
            preDiscount = { ...discountChange }
        } else {
            alert(i18n.t('messageAlertOver'))
            discountChange = { ...preDiscount }
        }
        this.setState(discountChange)
    }

    setModalVisible = (visible) => {
        this.setState({ modalVisible: false });
    }

    onFilterEvent = () => {

        const { priceFrom, priceTo, checkByDistant, checkByDiscount, discount, selectionCategory } = this.state

        console.log('Parameter')
        // console.log(props)
        const { onFilter } = this.props
        console.log(onFilter)

        let payload = {
            isFilter: false,
            withFromTo: false,
            withDiscount: false,
            withCategory: false,
            dataFromTo: {
                from: 0, to: 0,
            },
            discount: 0,
            categoryKey: 'none'
        }

        console.log('Payload 1')
        console.log(payload)

        if (checkByDiscount === true || checkByDistant === true || selectionCategory != 'none') payload.isFilter = true

        if (payload.isFilter === true) {
            if (checkByDistant === true) {
                payload.withFromTo = true
                payload.dataFromTo = {
                    from: priceFrom,
                    to: priceTo
                }
            }
            if (checkByDiscount === true) {
                payload.withDiscount = true
                payload.discount = discount
            }
            if (selectionCategory != 'none') {
                payload.withCategory = true
                payload.categoryKey = selectionCategory
            }
            console.log('Payload 2')
            console.log(payload)
        } 
        
        onFilter(payload)
        this.setState({ modalVisible: false })
    }

    render() {

        const { priceFrom, priceTo, checkByDistant, checkByDiscount, discount, selectionCategory } = this.state
        console.log(`OnRender Filter Model!`)
        console.log(this.props)

        const { onFilter } = this.props

        return (
            <Modal
                style={{ justifyContent: 'center', alignItems: 'center' }}
                animationInTiming={100}
                animationOutTiming={100}
                isVisible={this.state.modalVisible}
                onBackButtonPress={this.setModalVisible}
            >
                <View style={{
                    backgroundColor: 'white', height: 300, width: 300, borderRadius: 10,
                    justifyContent: 'center', alignItems: 'center'
                }}>
                    <View style={{
                        height: 40, width: 300,
                        borderTopLeftRadius: 10,
                        borderTopRightRadius: 10,
                        borderBottomWidth: 1,
                        borderBottomColor: 'red',
                        //elevation: 2,
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}>
                        <Text onPress={this.setModalVisible}
                            style={{ fontSize: 25, color: 'red', fontWeight: '500' }}
                        >{i18n.t('lableFilterBy')}</Text>
                    </View>

                    <View style={{ height: 220, backgroundColor: 'white', width: 300, }}>
                        <View style={{ flexDirection: 'row', opacity: 1 }}>
                            <Text style={{ marginLeft: 10, fontSize: 12, alignSelf: 'center', color: 'gray' }}>{i18n.t('lablePrice')}</Text>
                            <CheckBox
                                center
                                title={i18n.t('lableFromTo')}
                                checked={checkByDistant}
                                // checkedIcon='dot-circle-o'
                                // uncheckedIcon='circle-o'
                                onPress={() => this.setState({ checkByDistant: !checkByDistant })}
                                containerStyle={{ backgroundColor: 'white', borderColor: 'white', padding: 0 }}
                                textStyle={{ marginRight: 0, marginLeft: 2 }}
                                checkedColor='green'
                            />
                            <CheckBox
                                center
                                title={i18n.t('lableDiscountPercent')}
                                checked={checkByDiscount}
                                // checkedIcon='dot-circle-o'
                                // uncheckedIcon='circle-o'
                                onPress={() => this.setState({ checkByDiscount: !checkByDiscount })}
                                containerStyle={{ backgroundColor: 'white', borderColor: 'white', padding: 0 }}
                                textStyle={{ marginRight: 0, marginLeft: 2 }}
                                checkedColor='green'
                            />
                        </View>
                        <View style={{ borderWidth: 1, borderColor: 'red', marginRight: 20, marginLeft: 20, padding: 10 }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center', paddingBottom: 5 }}>
                                <View style={{ flex: 3, justifyContent: 'center', alignItems: 'flex-end' }}>
                                    <Text>{i18n.t('lableFrom')}</Text>
                                </View>
                                <TextInput
                                    value={(priceFrom === 0) ? '' : this.onPriceDisplay(priceFrom)}
                                    onChangeText={(price) => this.onPriceChange(1, price)}
                                    returnKeyType='done'
                                    keyboardType='numeric'
                                    placeholder="100$"
                                    style={{ flex: 8, marginLeft: 5 }}
                                    editable={checkByDistant}
                                />
                                {
                                    (priceFrom != 0) ? <Text style={{ marginRight: 30 }}>$</Text> : <View></View>
                                }
                            </View>
                            <View style={{ flexDirection: 'row', alignItems: 'center', paddingBottom: 5 }}>
                                <View style={{ flex: 3, justifyContent: 'center', alignItems: 'flex-end', }}>
                                    <Text>{i18n.t('lableTo')}</Text>
                                </View>
                                <TextInput
                                    value={(priceTo === 0) ? '' : this.onPriceDisplay(priceTo)}
                                    onChangeText={(price) => this.onPriceChange(2, price)}
                                    returnKeyType='done'
                                    keyboardType='numeric'
                                    placeholder="1.000.000$"
                                    style={{ flex: 8, marginLeft: 5 }}
                                    editable={checkByDistant}
                                />
                                {
                                    (priceTo != 0) ? <Text style={{ marginRight: 30 }}>$</Text> : <View></View>
                                }
                            </View>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <View style={{ flex: 3, justifyContent: 'center', alignItems: 'flex-end', }}>
                                    <Text>{i18n.t('lableDiscount')}</Text>
                                </View>
                                <TextInput
                                    value={(discount === 0) ? '' : discount.toString()}
                                    onChangeText={(discount) => this.onDiscountChange(discount)}
                                    returnKeyType='done'
                                    keyboardType='numeric'
                                    placeholder="20%"
                                    style={{ flex: (discount === 0) ? 1.5 : 1, marginLeft: 5 }}
                                    editable={checkByDiscount}
                                />
                                {
                                    (discount != 0) ? <Text style={{ flex: 7.2 }}>%</Text> : <View style={{ flex: 6.8 }}></View>
                                }
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            {/* <Text style={{ marginLeft: 10, fontSize: 16, alignSelf: 'center' }}>Category:</Text> */}
                            <Dropdown
                                label={i18n.t('lableCategoryCheckout')}
                                data={arrayItem}
                                containerStyle={{ flex: 1, marginLeft: 10 }}
                                overlayStyle={{ paddingLeft: 10 }}
                                // fontSize={12}
                                labelFontSize={12}
                                itemTextStyle={{ fontSize: 15 }}
                                value={selectionCategory}
                                onChangeText={(value, index) => {
                                    this.setState({ selectionCategory: value })
                                    // alert(value + ' ' + index)
                                }}
                            // itemPadding={10}
                            />
                        </View>
                    </View>

                    <View style={{
                        height: 40, backgroundColor: 'red', width: 300,
                        borderBottomLeftRadius: 10,
                        borderBottomRightRadius: 10,
                        borderTopWidth: 1,
                        borderTopColor: 'red',
                        elevation: 2,
                        flexDirection: 'row'
                    }}>
                        <TouchableOpacity
                            onPress={this.setModalVisible}
                            style={{
                                flex: 1, backgroundColor: 'white', borderBottomLeftRadius: 10,
                                elevation: 2,
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRightColor: 'red',
                                borderRightWidth: 1
                            }}>
                            <Text style={{ color: 'red', fontSize: 20, fontWeight: '500' }}>{i18n.t('lableCancel')}</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={this.onFilterEvent}
                            style={{
                                flex: 1, backgroundColor: 'white', borderBottomRightRadius: 10
                                , elevation: 2,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }} >
                            <Text style={{ color: 'red', fontSize: 20, fontWeight: '500' }}>{i18n.t('lableOK')}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        );
    }
}

mapDispatchToProps = (dispatch) => {
    return {
        onFilter: (payload) => {
            ActionCreator.ACTION_FILTER.payload = payload
            dispatch(ActionCreator.ACTION_FILTER)
        }
    }
}

export default connect(undefined, mapDispatchToProps)(FilterModal)