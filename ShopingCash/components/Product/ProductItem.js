import React from 'react';
import { StyleSheet, Text, View , Image, Dimensions, TouchableOpacity, Platform} from 'react-native';
import i18n from '../../i18n/i18n'

var {height, width} = Dimensions.get('window');

const char = `<>`

export default class ItemProduct extends React.PureComponent {
    
    render() {
      const {item, navigation} = this.props

      console.log('Product List')
      console.log(this.props)

      return (
        <View style={(Platform.OS === 'ios')?styles.containerItemIOS:styles.containerItemAndroid}> 
            <Text style={styles.itemName}>{item.name}</Text>
            <Image 
                style={styles.itemImage}
                source={{uri: item.image }}
                resizeMode='cover'
            />
            <Text style={styles.itemType}>{item.shortDescription}</Text>
            <Text style={styles.saleOffPrice}>{item.salePrice }$</Text>
            <Text style={styles.price}>{item.originalPrice}$</Text>
            <TouchableOpacity style={styles.buttonViewNow}
                              onPress={() => navigation.navigate('ProductDetail',{item: item, title: item.name})}
            >
                 <Text style={styles.textViewNow}>{i18n.t('buttonTitleViewNow')}</Text>
            </TouchableOpacity>
        </View>
      );
    }
  }

  const styles = StyleSheet.create({
    containerItemAndroid:{
      margin:5, 
      alignItems:'center',
      justifyContent:'center', 
      borderColor:'white',
      borderRadius:0.5,
      // shadowColor:'gray', shadowOpacity:0.5,
      // shadowOffset:{  width: 10,  height: 0  },
      // shadowRadius: 2,
      elevation: 3,
    },
    containerItemIOS:{
      margin:5,
      marginLeft:10,
      marginRight: 10, 
      alignItems:'center',
      justifyContent:'center', 
      borderColor:'lightgray',
      borderRadius:20,
      borderWidth:2,
      // shadowColor:'gray', shadowOpacity:0.5,
      // shadowOffset:{  width: 10,  height: 0  },
      // shadowRadius: 2,
      elevation: 3,
    },
    itemName: {
      fontSize:25, 
      marginBottom: 10, 
      color: 'rgb(67, 72, 77)', 
      fontWeight: "bold", 
      marginTop: 10,
      marginLeft: 5,
      marginRight:5, 
      textAlign:'center'
    },
    itemImage: {
      height:height /4, width:width * 0.7, marginBottom:10, borderRadius:0.5
    },
    itemType:{
      fontSize:20, marginBottom: 5, color:'black'
    },
    saleOffPrice:{
      fontSize:27, marginBottom: 5, color:'red', fontWeight: "bold"
    },
    price:{
      fontSize:20, marginBottom: 5, color:'gray', textDecorationLine:'line-through'
    },
    buttonViewNow:{
      height:50, width:width * 0.7, alignItems:'center', justifyContent:'center',backgroundColor:'red', marginBottom: 20 
    },
    textViewNow:{
      fontSize:20, fontWeight: "bold", color:'white' 
    }
}); 
