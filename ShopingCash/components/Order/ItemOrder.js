import React from 'react';
import { Text, View, TouchableOpacity, Image, Dimensions, Platform } from 'react-native';
let { height, width } = Dimensions.get('screen')
import i18n from '../../i18n/i18n'

export default class ItemOrder extends React.Component {

    // static navigationOptions = ({ navigation, tintColor }) => ({

    //     title: i18n.t('headerCart'),

    // })

    onPriceDisplay = (text) => {
        if ((text + '').length <= 3) return text + ''
        let i = 0
        let price = (text + '').split('')
        var priceToText = "";
        let count = 0
        for (i = price.length - 1; i >= 0; i--) {
            if (count === 3) {
                priceToText = price[i] + '.' + priceToText;
                count = 0;
            } else {
                priceToText = price[i] + priceToText
            }
            count++
        }
        return priceToText
    }

    render() {

        const { item, index , navigation} = this.props
        const { packageId, nameUser, payment, buyingDate, status } = item

        return (
            <View style={{
                height: 100, width: width * 0.98, flexDirection: 'row', justifyContent: 'center',
                alignItems: 'center', marginBottom: 10, elevation: 1, borderRadius:  (Platform.OS === 'ios') ? 10 : 2.5, borderColor: (Platform.OS === 'ios') ? 'lightgray' : 'white',
                marginLeft: 5, marginRight: 5, borderWidth: (Platform.OS === 'ios') ? 1 : 0,
            }}>
                <TouchableOpacity style={{flex:8, flexDirection:'column', justifyContent:'center', paddingLeft:10}}
                                  onPress={()=>navigation.navigate('OrderDetail',{item: item})}
                >
                    <Text>{i18n.t('lablePackageCode')}: {packageId}</Text>
                    <Text>{i18n.t('lableUsername')}: {nameUser}</Text>
                    <Text>{i18n.t('lablePayment')}: {payment}</Text>
                    <Text>{i18n.t('lablePurchasedate')}: {buyingDate}</Text>
                    <Text>{i18n.t('lableStatus')}: <Text style={{color:'green'}}>{status}</Text> </Text>
                </TouchableOpacity>

                 {/* <TouchableOpacity style={{flex:2,paddingTop:10, paddingBottom:10, paddingRight:5, backgroundColor:'red', borderRadius:5, marginLeft:5}}
                 >
                    <Text style={{color:'white', fontWeight:'bold', textAlign:'center'}}>Detail</Text>
                 </TouchableOpacity>    */}
                 
            </View>
        );
    }
}

