import React from 'react';
import { 
    Text, View,      
      TouchableOpacity,
       Dimensions, 
       ImageBackground ,
        StyleSheet,
        Alert
    } from 'react-native';
import call from 'react-native-phone-call'

import i18n from '../../i18n/i18n'

let {height, width} = Dimensions.get('screen')

const ContactInfo = () => {
    return (
        <TouchableOpacity style={styles.containerButton}
            onPress={()=>Alert.alert(
                i18n.t('contactTitle'),
                i18n.t('contactMessage'),
                [
                  {text:  i18n.t('cancelTitle'), onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                  {text: i18n.t('okTitle'), onPress: () => {
                    const args = {
                      number: '0949337629', // String value with the number to call
                      prompt: true // Optional boolean property. Determines if the user should be prompt prior to the call 
                    }
                    
                    call(args).catch(console.error)
                  }},
                ],
                { cancelable: false }
              )}
          >
          <Text style={styles.lable}>{i18n.t('lableButtonCall')}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    containerButton: {
        height:50, width:width*0.9, backgroundColor:'red', 
        alignItems:'center', justifyContent:'center',
        marginTop: 10, marginBottom: 15, borderRadius: 10,
        alignSelf:'center'
    }, 
    lable:{
        color:'white'
    }
})

export default ContactInfo