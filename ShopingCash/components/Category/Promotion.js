import React from 'react';
import { 
    Text, View,      
        StyleSheet,
    } from 'react-native';

const Promotion = () => {
    return (
     <View style={styles.containerPromotion}>
        <Text style={styles.title}
           >Chào mừng các bạn đã đến với OneNoTwo Shop!</Text>
         <Text>Khi đến đây các bạn sẽ được nhiều ưu đại và sự trải nghiệm mới mẻ từ các sản phẩm đa dạng của shop</Text>
         <Text>Một số ưu đãi không có thời hạn!</Text>
         <Text>- Mua 1 tặng 2 tính tiền 3 cái</Text>
         <Text>- Mua 2 tặng 4 tính tiền 6 cái</Text>
         <Text>- Mua 3 tặng 6 tính tiền 9 cái</Text>
         <Text>- Freeship cho bạn bè của Nhân</Text>
         <Text>Không mua không tính tiền!</Text>
         <Text>Chúc các bạn mua hàng vui vẻ!</Text>
     </View>
    )
}

const styles = StyleSheet.create({
    containerPromotion: {
        alignItems:'center'
    }, 
    title:{
        marginLeft:6,
    }
})

export default Promotion