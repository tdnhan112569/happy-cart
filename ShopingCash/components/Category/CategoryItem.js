import React from 'react';
import {
    Text, View,
    TouchableOpacity,
    Dimensions,
    ImageBackground,
    StyleSheet,
} from 'react-native';
import { NavigationActions, withNavigation } from 'react-navigation';
let { width } = Dimensions.get('screen')
import { connect } from 'react-redux'
import ActionCreator from '../../redux/action/ProductAction'

const CategoryItem = ({ item, index, navigation }) => {

    const { img, name } = item

    console.log('Item category')
    console.log(navigation)

    const filterCategory = () => {
        let payload = {
            isFilter: true,
            withFromTo: false,
            withDiscount: false,
            withCategory: true,
            dataFromTo: {
                from: 0, to: 0,
            },
            discount: 0,
            categoryKey: name
        }
        let actionFilter = {
            type: 'ACTION-FILTER',
            payload: payload
        }

        console.log(actionFilter)
        navigation.dispatch(actionFilter)
        navigation.navigate('ProductRouter')
    }

    return (
        <ImageBackground style={styles.imgBackgound}
            source={{ uri: img }}
        >
            <TouchableOpacity style={styles.viewCover}
                onPress={() => filterCategory()}
            >
                <Text style={styles.nameCategory}>{name}</Text>
            </TouchableOpacity>
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    imgBackgound: {
        height: 100, width: width / 2 - 20,
        backgroundColor: 'blue',
        marginLeft: 15,
        marginTop: 10,
        borderRadius: 10,
        //  shadowColor:'gray',
        //  shadowOffset:{
        //     height:2,width:0
        // },
        // shadowOpacity:5,
         elevation:1,
        // shadowRadius:10
    },
    viewCover: {
        flex: 1, justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(169, 169, 169, 0.5)'
    },
    nameCategory: {
        color: 'white', fontWeight: 'bold', fontSize: 20
    }

})

class CategoryItem2 extends React.Component {

    filterCategory = (name, navigation) => {
         
        const { onFilter } = this.props

        let payload = {
            isFilter: true,
            withFromTo: false,
            withDiscount: false,
            withCategory: true,
            dataFromTo: {
                from: 0, to: 0,
            },
            discount: 0,
            categoryKey: name
        }

        onFilter(payload)
        
        navigation.navigate('ProductRouter')
    }

    render() {

        const { item, navigation, index} = this.props
        const { img, name } = item;
        

        // alert( index)
        return (
            <ImageBackground style={styles.imgBackgound}
                source={{ uri: img }}
            >
                <TouchableOpacity style={styles.viewCover}
                    onPress={()=>this.filterCategory(name, navigation)}
                >
                    <Text style={styles.nameCategory}>{name}</Text>
                </TouchableOpacity>
            </ImageBackground>
        )
    }
}

mapDispatchToProps = (dispatch) => {
    return {
        onFilter: (payload) => {
            ActionCreator.ACTION_FILTER.payload = payload
            dispatch(ActionCreator.ACTION_FILTER)
        }
    }
}

export default connect(undefined, mapDispatchToProps)(CategoryItem2)
