import React from 'react';
import { 
    Text, View, Image,  Dimensions, 
        StyleSheet, TouchableOpacity
    } from 'react-native';
    let {width} = Dimensions.get('window')

const uriImg = 'https://blog.leotheme.com/wp-content/uploads/2018/12/best-Christmas-sale-Prestashop-discount.jpg'

const Sales = () => {
    return (
        <TouchableOpacity style={styles.btnContainer} disabled={true}>
            <Image style={styles.imgBanner}
                    source={{uri: uriImg}}
                    resizeMode='contain'    
                    />
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    imgBanner:{
        height:200, width: width - 20,  borderRadius: 10, 
    },
    btnContainer:{
        height:200,  width: width - 20, marginLeft: 10, marginRight: 10,borderRadius: 10, 
    }
})

export default Sales