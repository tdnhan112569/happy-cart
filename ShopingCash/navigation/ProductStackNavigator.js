import { createBottomTabNavigator, createAppContainer, createStackNavigator } from 'react-navigation';
import { Icon } from 'react-native-elements'
import React from 'react';

import ProduceScreen from '../screen/ProductScreen'
import ProductDetailScreen from '../screen/ProductDetailScreen'
import CartStack from './CartStackNavigator'
import CartScreen from '../screen/CartScreen'
import CheckoutScreen from '../screen/CheckoutScreen'
const ProductStack = createStackNavigator({
    Product:{
        screen: ProduceScreen,  
    },
    ProductDetail:{
        screen: ProductDetailScreen
    },
    Cart:{
        screen: CartScreen
    },
    Checkout: {
        screen: CheckoutScreen
    }
},
{
    
})

export default ProductStack