import { createBottomTabNavigator, createAppContainer, createStackNavigator } from 'react-navigation';
import OrderScreen from '../screen/OrderScreen'
import { Icon } from 'react-native-elements'
import React from 'react';

import OrderDetailScreen from '../screen/OrderDetailScreen'

const OrderStack = createStackNavigator({
    Order:{
        screen: OrderScreen,  
    },
    OrderDetail: {
        screen: OrderDetailScreen,  
    }
},
{

})




// const HomeContainer = createAppContainer(HomeStack)


export default OrderStack