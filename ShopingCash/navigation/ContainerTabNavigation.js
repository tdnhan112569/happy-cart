import React from 'react';
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import ProductScreen from '../screen/ProductScreen'
import OrderScreen from '../screen/OrderScreen'
import IntroduceScreen from '../screen/IntroduceScreen' 
import { Icon } from 'react-native-elements'
import i18n from '../i18n/i18n'

import HomeStack from './HomeStackNavigator'
import ProductStack from './ProductStackNavigator'
import IntroduceStack from './IntroduceStackNavigator'
import OrderStack from './OrderStackNavigator'
const TabNavigator = createBottomTabNavigator({
    HomeRouter: { 
        screen: HomeStack,
        navigationOptions:{
            tabBarIcon: ({tintColor}) => (
                <Icon 
                   name='home'
                   type='Feather'
                   color={tintColor}
                />
             ),
             title: i18n.t('tab1Name')
        }
    },
    ProductRouter: {
        screen: ProductStack,
        navigationOptions:{
            title:i18n.t('tab2Name'),
            tabBarIcon: ({tintColor}) => (
             <Icon 
                name='list'
                type='Entypo'
                color={tintColor}
             />
           )
        }
    },
    OrderRouter: {
        screen: OrderStack,
        navigationOptions:{
            title:i18n.t('tab3Name'),
            tabBarIcon: ({tintColor}) => (
            <Icon 
                name='history'
                type='FontAwesome5'
                color={tintColor}
            />
             )
        }
    },
    IntroduceRouter: {
       screen: IntroduceStack,
       navigationOptions:{
        title:i18n.t('tab4Name'),
        tabBarIcon: ({tintColor}) => (
            <Icon 
               name='info'
               type='SimpleLineIcons'
               color={tintColor}
            />
          )
       }
    }
  },
  {
    initialRouteName:"HomeRouter",
    lazy:true,
    tabBarPosition:'bottom',
    animationEnabled: true,
    swipeEnabled:true,
    tabBarOptions: {
        activeTintColor:'white',
        activeBackgroundColor:'red',
        inactiveTintColor:'gray',
        inactiveBackgroundColor:'white',
        labelStyle:{
            fontSize:15,
        },
        style:{
           borderTopWidth:1,
           shadowColor:'black',
           shadowOffset:{
             height:2,width:0
          },
          shadowOpacity:0.5,elevation:0.5,
        }
    },
  });

const  TabNavigatorContainer = createAppContainer(TabNavigator)
export default TabNavigatorContainer
