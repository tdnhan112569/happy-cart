import { createBottomTabNavigator, createAppContainer, createStackNavigator } from 'react-navigation';
import CartScreen from '../screen/CartScreen'
import { Icon } from 'react-native-elements'
import React from 'react';

const CartStack = createStackNavigator({
    Cart:{
        screen: CartScreen,  
    }
},
{

})

export default CartStack