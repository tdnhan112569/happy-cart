import { createBottomTabNavigator, createAppContainer, createStackNavigator } from 'react-navigation';
import HomeScreen from '../screen/HomeScreen'
import { Icon } from 'react-native-elements'
import React from 'react';
import CartStack from './CartStackNavigator'

const HomeStack = createStackNavigator({
    Home:{
        screen: HomeScreen,  
    },
    CartRouter:{
        screen: CartStack
    }
},
{
    // tabBarOptions: {
    //     tabBarIcon: ({tintColor}) => (
    //         <Icon 
    //            name='home'
    //            type='Feather'
    //            color='white'
    //         />
    //      )
    // }
    
})




// const HomeContainer = createAppContainer(HomeStack)


export default HomeStack