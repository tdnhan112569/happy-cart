import React from 'react';
import { Text, View, Dimensions, FlatList, ScrollView, TouchableOpacity, Alert } from 'react-native';
import i18n from '../i18n/i18n'
import { Icon } from 'react-native-elements'
import { Constants } from 'expo';
let { height, width } = Dimensions.get('screen')
import { connect } from 'react-redux'
import ActionCartCreator from '../redux/action/CartAction'

import ItemCart from '../components/Cart/ItemCart'

class CheckoutScreen extends React.Component {

  static navigationOptions = ({ navigation, tintColor }) => ({
    title: i18n.t('headerCheckout'),
    header: (
      <View style={{ height: 50 + Constants.statusBarHeight, backgroundColor: 'red', flexDirection: 'row', paddingTop: Constants.statusBarHeight }}>
        <TouchableOpacity style={{ height: 50, flex: 2, backgroundColor: 'red', justifyContent: 'center', alignItems: 'center', paddingLeft: 5 }}
          onPress={() => navigation.goBack()}
        >
          <Text style={{ color: 'white', fontSize: 18 }}>{i18n.t('buttonBackTitle')}</Text>
        </TouchableOpacity>
        <View style={{ flex: 8.5, height: 50, alignItems: 'center', justifyContent: 'center', backgroundColor: 'red' }}>
          <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>{i18n.t('headerCheckout')}</Text>
        </View>
        <View style={{ height: 50, flex: 2, backgroundColor: 'red', justifyContent: 'center', alignItems: 'center' }}>
        </View>
      </View>
    )
  })

  _renderItem = ({ item, index }) => {
    return <ItemCart item={item} index={index} />
  }

  getTotalPrice = (arrayItem) => {
    let total = 0
    for (let i = 0; i < arrayItem.length; i++) {
      total += arrayItem[i].totalPrice
    }
    return total
  }

  onPriceDisplay = (text) => {
    if ((text + '').length <= 3) return text + ''
    let i = 0
    let price = (text + '').split('')
    var priceToText = "";
    let count = 0
    for (i = price.length - 1; i >= 0; i--) {
      if (count === 3) {
        priceToText = price[i] + '.' + priceToText;
        count = 0;
      } else {
        priceToText = price[i] + priceToText
      }
      count++
    }
    return priceToText
  }

  onBuyPackage = (packageId, nameUser, payment, buyingDate, item) => {

    let payload = {
      packageId: packageId,
      nameUser: nameUser,
      payment: payment,
      buyingDate: buyingDate,
      status: 'Ordered',
      listItem: item
    }

    console.log('Package sent to reducer')
    console.log(payload)

    const { onAddToCard, navigation } = this.props

    console.log(this.props)
    Alert.alert(
      i18n.t('lableConfirm'),
      i18n.t('lableMessageDialogBuy'),
      [
        { text: i18n.t('cancelTitle'), onPress: () => console.log('Cancel order'), style: 'cancel' },
        {
          text: i18n.t('lableContinute'), onPress: () => {
            onAddToCard(payload)
            navigation.popToTop()
            Alert.alert(i18n.t('lableStatus'), i18n.t('lableThank'),
              [
                { text: i18n.t('lableBuyMore'), onPress: () => { }, style: 'cancel' },
                { text: i18n.t('lableCheckPackage'), onPress: () => navigation.navigate('OrderRouter'), style: 'default' }
              ]
            )
          }
        },
      ],
      { cancelable: false }
    )
  }

  render() {
    const { navigation } = this.props
    const item = navigation.getParam('item', {})
    console.log('Checkout screen')
    console.log(item)
    const showTime = new Date()
    console.log('PK' + showTime.getDate() + '   ' + showTime.getDay())
    var j = new Array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
    const totalPrice = this.getTotalPrice(item)

    const packageId = 'PK' + showTime.getTime(), nameUser = Constants.deviceName,
      payment = 'Cash on Delivery (COD)', buyingDate = j[showTime.getDay() - 1] + ', ' + showTime.toLocaleDateString()


    return (
      <ScrollView>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 10 }}>
          {/* <Text
            onPress={() => { this.props.navigation.navigate('OrderRouter') }}
          >I try to do it</Text> */}

          <View style={{ flexDirection: 'row', height: 40, borderWidth: 1, borderColor: 'gray', marginLeft: 15, marginRight: 15, marginBottom: 5 }}>
            <View style={{ flex: 4, backgroundColor: 'lightgray', justifyContent: 'center', paddingLeft: 10 }}>
              <Text>{i18n.t('lablePackageCode')}</Text>
            </View>
            <View style={{ flex: 7, justifyContent: 'center', paddingLeft: 10 }}>
              <Text>{packageId}</Text>
            </View>
          </View>

          <View style={{ flexDirection: 'row', height: 40, borderWidth: 1, borderColor: 'gray', marginLeft: 15, marginRight: 15, marginBottom: 5 }}>
            <View style={{ flex: 4, backgroundColor: 'lightgray', justifyContent: 'center', paddingLeft: 10 }}>
              <Text>{i18n.t('lableUsername')}</Text>
            </View>
            <View style={{ flex: 7, justifyContent: 'center', paddingLeft: 10 }}>
              <Text>{nameUser}</Text>
            </View>
          </View>

          <View style={{ flexDirection: 'row', height: 40, borderWidth: 1, borderColor: 'gray', marginLeft: 15, marginRight: 15, marginBottom: 5 }}>
            <View style={{ flex: 4, backgroundColor: 'lightgray', justifyContent: 'center', paddingLeft: 10 }}>
              <Text>{i18n.t('lablePayment')}</Text>
            </View>
            <View style={{ flex: 7, justifyContent: 'center', paddingLeft: 10 }}>
              <Text>{payment}</Text>
            </View>
          </View>

          <View style={{ flexDirection: 'row', height: 40, borderWidth: 1, borderColor: 'gray', marginLeft: 15, marginRight: 15, marginBottom: 5 }}>
            <View style={{ flex: 4, backgroundColor: 'lightgray', justifyContent: 'center', paddingLeft: 10 }}>
              <Text>{i18n.t('lablePurchasedate')}</Text>
            </View>
            <View style={{ flex: 7, justifyContent: 'center', paddingLeft: 10 }}>
              <Text>{buyingDate}</Text>
            </View>
          </View>

          <View style={{ width: width * 0.9, borderWidth: 1, borderColor: 'black', alignSelf: 'center', marginBottom: 10, marginTop: 10 }}></View>

          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontSize: 20, marginBottom: 10 }}>{i18n.t('lableDetailPackage')}</Text>
            <FlatList
              data={item}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index + ''}
              style={{ flex: 1, width: width }}
            />
          </View>

          <View style={{ width: width * 0.9, borderWidth: 1, borderColor: 'black', alignSelf: 'center', marginBottom: 10, marginTop: 10 }}></View>

          <View style={{ flexDirection: 'row', width: width * 0.9, height: 40, justifyContent: 'space-evenly', alignItems: 'center' }}>
            <Text style={{ fontSize: 20, color: 'red', }}>{i18n.t('lableTotal')}</Text>
            <Text style={{ fontSize: 25, color: 'red', fontWeight: 'bold' }}>{this.onPriceDisplay(totalPrice)}$</Text>
          </View>

          <View style={{ width: width * 0.9, borderWidth: 1, borderColor: 'black', alignSelf: 'center', marginBottom: 10, marginTop: 10 }}></View>

          <TouchableOpacity style={{
            height: 50, width: width * 0.8, alignSelf: 'center', backgroundColor: 'red',
            justifyContent: 'center', alignItems: 'center', borderRadius: 10, marginBottom: 10
          }}
            onPress={() => this.onBuyPackage(packageId, nameUser, payment, buyingDate, item)}
          >
            <Text style={{ fontSize: 25, color: 'white', fontWeight: 'bold' }}>{i18n.t('lableButtonBuyIt')}</Text>
          </TouchableOpacity>

        </View>
      </ScrollView>
    );
  }
}

mapDispatchToProps = (dispatch) => {
  return {
    onAddToCard: (payload) => {
      ActionCartCreator.ACTION_CHECKOUT_CART.payload = payload
      dispatch(ActionCartCreator.ACTION_CHECKOUT_CART)
    }
  }
}

export default connect(undefined, mapDispatchToProps)(CheckoutScreen)





 // tabBarIcon: ({tintColor}) => (
    //   <Icon
    //      name='history'
    //      type='FontAwesome5'
    //      color='white'
    //   />
    // )