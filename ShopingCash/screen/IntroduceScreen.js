import React from 'react';
import { Text, View, ScrollView, Platform } from 'react-native';
import i18n from '../i18n/i18n'
import { Icon } from 'react-native-elements'
import { Constants } from 'expo';

export default class IntroduceScreen extends React.Component {

  static navigationOptions = ({ navigation, tintColor }) => ({

    title: i18n.t('tab4Name'),
    header: (
      <View style={{ height: 50 + Constants.statusBarHeight, backgroundColor: 'red', flexDirection: 'row', paddingTop: Constants.statusBarHeight }}>
        <View style={{ flex: 7, height: 50, alignItems: 'center', justifyContent: 'center', backgroundColor: 'red' }}>
          <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>{i18n.t('tab4Name')}</Text>
        </View>
      </View>
    )
  })

  render() {
    console.log(Constants)

    const { manifest, platform } = Constants
    const { android, ios } = platform

    return (
      <ScrollView style={{ flex: 1, paddingTop: 5, paddingBottom: 5 }}>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>

          <View style={{ flexDirection: 'row', height: 40, borderWidth: 1, borderColor: 'gray', marginLeft: 15, marginRight: 15, marginBottom: 5 }}>
            <View style={{ flex: 4, backgroundColor: 'lightgray', justifyContent: 'center', paddingLeft: 10 }}>
              <Text>appOwnership</Text>
            </View>
            <View style={{ flex: 7, justifyContent: 'center', paddingLeft: 10 }}>
              <Text>{Constants.appOwnership}</Text>
            </View>
          </View>

          <View style={{ flexDirection: 'row', height: 40, borderWidth: 1, borderColor: 'gray', marginLeft: 15, marginRight: 15, marginBottom: 5 }}>
            <View style={{ flex: 4, backgroundColor: 'lightgray', justifyContent: 'center', paddingLeft: 10 }}>
              <Text>name</Text>
            </View>
            <View style={{ flex: 7, justifyContent: 'center', paddingLeft: 10 }}>
              <Text>{manifest.name}</Text>
            </View>
          </View>

          <View style={{ flexDirection: 'row', height: 40, borderWidth: 1, borderColor: 'gray', marginLeft: 15, marginRight: 15, marginBottom: 5 }}>
            <View style={{ flex: 4, backgroundColor: 'lightgray', justifyContent: 'center', paddingLeft: 10 }}>
              <Text>orientation</Text>
            </View>
            <View style={{ flex: 7, justifyContent: 'center', paddingLeft: 10 }}>
              <Text>{manifest.orientation}</Text>
            </View>
          </View>

          <View style={{ flexDirection: 'row', height: 40, borderWidth: 1, borderColor: 'gray', marginLeft: 15, marginRight: 15, marginBottom: 5 }}>
            <View style={{ flex: 4, backgroundColor: 'lightgray', justifyContent: 'center', paddingLeft: 10 }}>
              <Text>expoVersion</Text>
            </View>
            <View style={{ flex: 7, justifyContent: 'center', paddingLeft: 10 }}>
              <Text >{Constants.expoVersion}</Text>
            </View>
          </View>

          <View style={{ flexDirection: 'row', height: 40, borderWidth: 1, borderColor: 'gray', marginLeft: 15, marginRight: 15, marginBottom: 5 }}>
            <View style={{ flex: 4, backgroundColor: 'lightgray', justifyContent: 'center', paddingLeft: 10 }}>
              <Text>platform</Text>
            </View>
            <View style={{ flex: 7, justifyContent: 'center', paddingLeft: 10 }}>
              <Text>{(android != undefined) ? 'android' : 'ios'}</Text>
            </View>
          </View>

          <View style={{ flexDirection: 'row', height: 40, borderWidth: 1, borderColor: 'gray', marginLeft: 15, marginRight: 15, marginBottom: 5 }}>
            <View style={{ flex: 4, backgroundColor: 'lightgray', justifyContent: 'center', paddingLeft: 10 }}>
              <Text>sdkVersion</Text>
            </View>
            <View style={{ flex: 7, justifyContent: 'center', paddingLeft: 10 }}>
              <Text>{manifest.sdkVersion}</Text>
            </View>
          </View>

          <View style={{ flexDirection: 'row', height: 40, borderWidth: 1, borderColor: 'gray', marginLeft: 15, marginRight: 15, marginBottom: 5 }}>
            <View style={{ flex: 4, backgroundColor: 'lightgray', justifyContent: 'center', paddingLeft: 10 }}>
              <Text>deviceName</Text>
            </View>
            <View style={{ flex: 7, justifyContent: 'center', paddingLeft: 10 }}>
              <Text>{Constants.deviceName}</Text>
            </View>
          </View>

          {(Platform.OS === 'android') ?
            <View style={{ flexDirection: 'row', height: 40, borderWidth: 1, borderColor: 'gray', marginLeft: 15, marginRight: 15, marginBottom: 5 }}>
              <View style={{ flex: 4, backgroundColor: 'lightgray', justifyContent: 'center', paddingLeft: 10 }}>
                <Text>systemVersion</Text>
              </View>
              <View style={{ flex: 7, justifyContent: 'center', paddingLeft: 10 }}>
                <Text>{Constants.systemVersion}</Text>
              </View>
            </View>
            : <View></View>
          }

          <View style={{ flexDirection: 'row', height: 40, borderWidth: 1, borderColor: 'gray', marginLeft: 15, marginRight: 15, marginBottom: 5 }}>
            <View style={{ flex: 4, backgroundColor: 'lightgray', justifyContent: 'center', paddingLeft: 10 }}>
              <Text>deviceYearClass</Text>
            </View>
            <View style={{ flex: 7, justifyContent: 'center', paddingLeft: 10 }}>
              <Text>{Constants.deviceYearClass}</Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row', height: 40, borderWidth: 1, borderColor: 'gray', marginLeft: 15, marginRight: 15, marginBottom: 5 }}>
            <View style={{ flex: 4, backgroundColor: 'lightgray', justifyContent: 'center', paddingLeft: 10 }}>
              <Text>isDevice</Text>
            </View>
            <View style={{ flex: 7, justifyContent: 'center', paddingLeft: 10 }}>
              <Text>{(Constants.isDevice === true) ? 'True' : 'false'}</Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row', height: 40, borderWidth: 1, borderColor: 'gray', marginLeft: 15, marginRight: 15, marginBottom: 10 }}>
            <View style={{ flex: 4, backgroundColor: 'lightgray', justifyContent: 'center', paddingLeft: 10 }}>
              <Text>statusBarHeight</Text>
            </View>
            <View style={{ flex: 7, justifyContent: 'center', paddingLeft: 10 }}>
              <Text>{Constants.statusBarHeight}</Text>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}





 // tabBarIcon: ({tintColor}) => (
    //   <Icon 
    //      name='history'
    //      type='FontAwesome5'
    //      color='white'
    //   />
    // )
  //   <View style={{ flexDirection: 'row', height: 40, borderWidth: 1, borderColor: 'gray', marginLeft: 15, marginRight: 15, marginBottom: 5 }}>
  //   <View style={{ flex: 4, backgroundColor: 'lightgray', justifyContent: 'center', paddingLeft: 10 }}>
  //     <Text>platform</Text>
  //   </View>
  //   <View style={{ flex: 7, justifyContent: 'center', paddingLeft: 10 }}>
  //     <Text>{Constants.platform}</Text>
  //   </View>
  // </View>

//   <View style={{ flexDirection: 'row', height: 40, borderWidth: 1, borderColor: 'gray', marginLeft: 15, marginRight: 15, marginBottom: 5 }}>
//   <View style={{ flex: 4, backgroundColor: 'lightgray', justifyContent: 'center', paddingLeft: 10 }}>
//     <Text>systemFonts</Text>
//   </View>
//   <View style={{ flex: 7, justifyContent: 'center', paddingLeft: 10 }}>
//     <Text>{Constants.systemFonts}</Text>
//   </View>
// </View>

// <View style={{ flexDirection: 'row', height: 40, borderWidth: 1, borderColor: 'gray', marginLeft: 15, marginRight: 15, marginBottom: 5 }}>
// <View style={{ flex: 4, backgroundColor: 'lightgray', justifyContent: 'center', paddingLeft: 10 }}>
//   <Text>sessionId</Text>
// </View>
// <View style={{ flex: 7, justifyContent: 'center', paddingLeft: 10 }}>
//   <Text>{Constants.sessionId}</Text>
// </View>
// </View>


// <View style={{ flexDirection: 'row', height: 40, borderWidth: 1, borderColor: 'gray', marginLeft: 15, marginRight: 15, marginBottom: 5 }}>
// <View style={{ flex: 4, backgroundColor: 'lightgray', justifyContent: 'center', paddingLeft: 10 }}>
//   <Text>manifest</Text>
// </View>
// <View style={{ flex: 7, justifyContent: 'center', paddingLeft: 10 }}>
//   <Text>{Constants.manifest}</Text>
// </View>
// </View>
// <Text
// onPress={() => { this.props.navigation.navigate('HomeRouter') }}
// >I try to do it</Text>