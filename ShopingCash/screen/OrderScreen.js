import React from 'react';
import { Text, View, FlatList, Dimensions, ScrollView } from 'react-native';
import i18n from '../i18n/i18n'
import { Icon } from 'react-native-elements'
import { connect } from 'react-redux'
let { height, width } = Dimensions.get('screen')
import { Constants } from 'expo'
import ItemOrder from '../components/Order/ItemOrder'
class OrderScreen extends React.Component {

  constructor(props) {
    super(props)
  }

  static navigationOptions = ({ navigation, tintColor }) => ({
    title: i18n.t('tab3Name'),
    header: (
      <View style={{ height: 50 + Constants.statusBarHeight, backgroundColor: 'red', flexDirection: 'row', paddingTop: Constants.statusBarHeight }}>
        <View style={{ flex: 7, height: 50, alignItems: 'center', justifyContent: 'center', backgroundColor: 'red' }}>
          <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>{i18n.t('tab3Name')}</Text>
        </View>
      </View>
    )
  })

  _renderItem = ({ item, index }) => {
    return <ItemOrder item={item} index={index} navigation={this.props.navigation}/>
  }

  render() {
    console.log('On Render Order History Screen')
    console.log(this.props)
    //alert(JSON.stringify(this.props.orderList))
    const { orderList, navigation } = this.props
    return (

      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 5 }}>
        {
          (orderList.length > 0) ?
            <FlatList
              data={orderList.reverse()}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index + ''}
              style={{ flex: 1, width: width }}
            />
            : <Text style={{ color: 'red', fontSize: 20 }}>{i18n.t('lableHistoryOrderEmpty')}</Text>
        }

      </View>
    );
  }
}

mapStateToProps = (state) => {
  return {
    orderList: state.cartRed.historyOrderList,
  }
}

export default connect(mapStateToProps)(OrderScreen)