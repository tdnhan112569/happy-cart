import React from 'react';
import { Text, View, Dimensions, FlatList, ScrollView, TouchableOpacity, Alert } from 'react-native';
import i18n from '../i18n/i18n'
import { Icon } from 'react-native-elements'
import { Constants } from 'expo';
let { height, width } = Dimensions.get('screen')
import { connect } from 'react-redux'
import ActionCartCreator from '../redux/action/CartAction'

import ItemCart from '../components/Cart/ItemCart'

export default class OrderDetailScreen extends React.Component {

  static navigationOptions = ({ navigation, tintColor }) => ({
    title: i18n.t('lableOrderDetailScreen'),
    header: (
      <View style={{ height: 50 + Constants.statusBarHeight, backgroundColor: 'red', flexDirection: 'row', paddingTop: Constants.statusBarHeight }}>
        <TouchableOpacity style={{ height: 50, flex: 2, backgroundColor: 'red', justifyContent: 'center', alignItems: 'center', paddingLeft: 5 }}
          onPress={() => navigation.goBack()}
        >
          <Text style={{ color: 'white', fontSize: 18 }}>{i18n.t('buttonBackTitle')}</Text>
        </TouchableOpacity>
        <View style={{ flex: 8.5, height: 50, alignItems: 'center', justifyContent: 'center', backgroundColor: 'red' }}>
          <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>{i18n.t('lableOrderDetailScreen')}</Text>
        </View>
        <View style={{ height: 50, flex: 2, backgroundColor: 'red', justifyContent: 'center', alignItems: 'center' }}>
        </View>
      </View>
    )
  })

  _renderItem = ({ item, index }) => {
    return <ItemCart item={item} index={index} />
  }

  getTotalPrice = (arrayItem) => {
    let total = 0
    for (let i = 0; i < arrayItem.length; i++) {
      total += arrayItem[i].totalPrice
    }
    return total
  }

  onPriceDisplay = (text) => {
    if ((text + '').length <= 3) return text + ''
    let i = 0
    let price = (text + '').split('')
    var priceToText = "";
    let count = 0
    for (i = price.length - 1; i >= 0; i--) {
      if (count === 3) {
        priceToText = price[i] + '.' + priceToText;
        count = 0;
      } else {
        priceToText = price[i] + priceToText
      }
      count++
    }
    return priceToText
  }

  render() {
    const { navigation } = this.props
    const item = navigation.getParam('item', {})
    const { packageId, nameUser, payment, buyingDate, status, listItem } = item

    const totalPrice = this.getTotalPrice(listItem)

    return (
      <ScrollView>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' , paddingTop:10}}>
          {/* <Text
            onPress={() => { this.props.navigation.navigate('OrderRouter') }}
          >I try to do it</Text> */}

          <View style={{ flexDirection: 'row', height: 40, borderWidth: 1, borderColor: 'gray', marginLeft: 15, marginRight: 15, marginBottom: 5 }}>
            <View style={{ flex: 4, backgroundColor: 'lightgray', justifyContent: 'center', paddingLeft: 10 }}>
              <Text>{i18n.t('lablePackageCode')}</Text>
            </View>
            <View style={{ flex: 7, justifyContent: 'center', paddingLeft: 10 }}>
              <Text>{packageId}</Text>
            </View>
          </View>

          <View style={{ flexDirection: 'row', height: 40, borderWidth: 1, borderColor: 'gray', marginLeft: 15, marginRight: 15, marginBottom: 5 }}>
            <View style={{ flex: 4, backgroundColor: 'lightgray', justifyContent: 'center', paddingLeft: 10 }}>
              <Text>{i18n.t('lableUsername')}</Text>
            </View>
            <View style={{ flex: 7, justifyContent: 'center', paddingLeft: 10 }}>
              <Text>{nameUser}</Text>
            </View>
          </View>

          <View style={{ flexDirection: 'row', height: 40, borderWidth: 1, borderColor: 'gray', marginLeft: 15, marginRight: 15, marginBottom: 5 }}>
            <View style={{ flex: 4, backgroundColor: 'lightgray', justifyContent: 'center', paddingLeft: 10 }}>
              <Text>{i18n.t('lablePayment')}</Text>
            </View>
            <View style={{ flex: 7, justifyContent: 'center', paddingLeft: 10 }}>
              <Text>{payment}</Text>
            </View>
          </View>

          <View style={{ flexDirection: 'row', height: 40, borderWidth: 1, borderColor: 'gray', marginLeft: 15, marginRight: 15, marginBottom: 5 }}>
            <View style={{ flex: 4, backgroundColor: 'lightgray', justifyContent: 'center', paddingLeft: 10 }}>
              <Text>{i18n.t('lablePurchasedate')}</Text>
            </View>
            <View style={{ flex: 7, justifyContent: 'center', paddingLeft: 10 }}>
              <Text>{buyingDate}</Text>
            </View>
          </View>

          <View style={{ flexDirection: 'row', height: 40, borderWidth: 1, borderColor: 'gray', marginLeft: 15, marginRight: 15, marginBottom: 5 }}>
            <View style={{ flex: 4, backgroundColor: 'lightgray', justifyContent: 'center', paddingLeft: 10 }}>
              <Text>{i18n.t('lableStatus')}</Text>
            </View>
            <View style={{ flex: 7, justifyContent: 'center', paddingLeft: 10 }}>
              <Text style={{color:'green'}}>{status}</Text>
            </View>
          </View>

          <View style={{ width: width * 0.9, borderWidth: 1, borderColor: 'black', alignSelf: 'center', marginBottom: 10, marginTop: 10 }}></View>

          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontSize: 20, marginBottom: 10 }}>{i18n.t('lableDetailPackage')}</Text>
            <FlatList
              data={listItem}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index + ''}
              style={{ flex: 1, width: width }}
            />
          </View>

          <View style={{ width: width * 0.9, borderWidth: 1, borderColor: 'black', alignSelf: 'center', marginBottom: 10, marginTop: 10 }}></View>

          <View style={{ flexDirection: 'row', width: width * 0.9, height: 40, justifyContent: 'space-evenly', alignItems: 'center' }}>
            <Text style={{ fontSize: 20, color: 'red', }}>{i18n.t('lableTotal')}</Text>
            <Text style={{ fontSize: 25, color: 'red', fontWeight: 'bold' }}>{this.onPriceDisplay(totalPrice)}$</Text>
          </View>

          <View style={{ width: width * 0.9, borderWidth: 1, borderColor: 'black', alignSelf: 'center', marginBottom: 10, marginTop: 10 }}></View>
          

        </View>
      </ScrollView>
    );
  }
}
