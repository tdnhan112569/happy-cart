import React from 'react';
import { Text, View, FlatList, TouchableOpacity, Dimensions, ImageBackground, Alert, ScrollView, StyleSheet } from 'react-native';
import i18n from '../i18n/i18n'
import { Icon } from 'react-native-elements'
import Category from '../API/fakeData/CategoryData'
import { onNavigateToProduceRouter } from '../navigation/HomeStackNavigator'
let { height, width } = Dimensions.get('screen')
import CategoryItem from '../components/Category/CategoryItem'
import Promotion from '../components/Category/Promotion'
import ContactInfo from '../components/Category/ContantInfo'
import Sales from '../components/Category/Sales'
import { Constants } from 'expo'
class HomeScreen extends React.Component {

  static navigationOptions = ({ navigation, tintColor }) => ({
    title: i18n.t('tab1Name'),
    header: (
      <View style={{ height: 50 + Constants.statusBarHeight, backgroundColor: 'red', flexDirection: 'row', paddingTop: Constants.statusBarHeight }}>
        <View style={{ flex: 7, height: 50, alignItems: 'center', justifyContent: 'center', backgroundColor: 'red' }}>
          <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>{i18n.t('tab1Name')}</Text>
        </View>
      </View>
    )
  })


  componentWillMount() {

  }

  _renderItem = ({ item, index }) => {
    return (
      <CategoryItem item={item} index={index} navigation={this.props.navigation}/>
    )
  }

  render() {

    const { navigation } = this.props
    return (
      <ScrollView>
        <View style={styles.container}>
          {/* Promotion */}
          <Text style={styles.lableTitle}>{i18n.t('lableIntroduce')}</Text>
          <Promotion />

          {/* Advertiment*/}
          <Text style={styles.lableTitle}>{i18n.t('lableBanner')}</Text>
          <Sales />
          {/*<Text onPress={()=> this.props.navigation.navigate('Cart')}>Go to cart</Text>*/}
          {/* List Category*/}
          <Text style={styles.lableTitle}>{i18n.t('lableCategory')}</Text>
          <FlatList
            data={Category.body}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => index}
            numColumns={2}
          />

          <View style={styles.div}></View>
          {/*Contact */}
          <Text style={styles.lableTitle}>{i18n.t('lableContact')}</Text>
          <ContactInfo />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  lableTitle: {

    fontSize: 25, marginLeft: 3, fontWeight: 'bold'
  },
  container: {
    flex: 1, justifyContent: 'center', alignItems: 'center',
  },
  div: {
    marginTop: 15
  }
})

export default HomeScreen