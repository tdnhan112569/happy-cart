import React from 'react';
import {
  Text, View, Platform,
  StyleSheet, TouchableOpacity,
  Dimensions, FlatList,
  ScrollView
} from 'react-native';
import i18n from '../i18n/i18n'
import { Icon } from 'react-native-elements'
import { SearchBar } from 'react-native-elements'
let { height, width } = Dimensions.get('screen')
import ActionCreator from '../redux/action/ProductAction'
import { connect } from 'react-redux'
import ItemProduct from '../components/Product/ProductItem'
import Product from '../API/fakeData/ProductData'
import Category from '../API/fakeData/CategoryData'
import FilterModal from '../components/Product/FilterModel';

class ProductScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showModel: false,
      filter: {
        price: 520
      },
      keySearch: ''
    }
  }

  static navigationOptions = ({ navigation, tintColor }) => ({
    headerTitle: 'Product',
    header: null
    // header: (
    //   <View style={{ backgroundColor: 'red', flexDirection: 'row', paddingTop: (Platform.OS === 'ios') ? 10 : 20 }}>
    //     {
    //       Platform.OS === 'android' ?
    //         <SearchBar
    //           showLoading={false}
    //           platform="android"
    //           placeholder='Product name'
    //           containerStyle={{ backgroundColor: 'red', width: width - 50 }}
    //           inputStyle={{ color: 'white' }}
    //           clearIcon={{ type: 'MaterialIcons', name: 'clear' }}
    //           rightIconContainerStyle={{ marginRight: 50 }}
    //           onChangeText={(Text) => {
    //             console.log(Text)
    //           }}
    //         /> :

    //         <SearchBar
    //           showLoading={false}
    //           platform="ios"
    //           cancelButtonTitle="Cancel"
    //           placeholder='Search'
    //           containerStyle={{ backgroundColor: 'red', width: width - 50 }}
    //           inputStyle={{ color: 'white' }}
    //         //  rightIconContainerStyle={{ marginRight: 50 }}
    //         // cancelButtonProps={undefined}
    //         />
    //     }
    //     <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
    //       <Icon
    //         color='white'
    //         name='shopping-cart'
    //         type='MaterialIcons'
    //         size={35}
    //         onPress={() => navigation.navigate('Cart')}
    //       />
    //     </View>
    //   </View>
    // ),
  })

  componentWillMount() {
    this.props.onCreate()
  }

  componentDidMount() {

  }

  _renderItem = ({ item, index }) => {
    return (
      <ItemProduct item={item} index={index} navigation={this.props.navigation} />
    )
  }

  switchToProductDetail = () => {
    //this.setState({ filter: { price: 200 } })
    (!this.state.showModel) ? this.setState({ showModel: true }) :
      this.setState({ showModel: false })
  }

  getNameCategoryById = (id) => {
    const { body } = Category
    let object = body.filter((item) => {
      if (item.id === id) return item
    })
    return object[0].name
  }

  checkPushToProductList = (arryObject = [], object = {}) => {
    for (let i = 0; i < arryObject.length; i++) {
      if (arryObject[i].id === object.id) return false
    }
    return true
  }

  checkInclucesProduct = (arryObject = [], object = {}) => {
    for (let i = 0; i < arryObject.length; i++) {
      if (arryObject[i].id === object.id) return true
    }
    return false
  }

  onSearch = (jsonArr, context) => {
    const tokens = (context + '').toLowerCase().split(' ');
    let results = [];
    const calcScore = (obj) => {
      // calculate score of searching
      // +1 if a word in tokens exists in obj.name
      // otherwise, +0
      let score = 0;
      for (let token of tokens) {
        score += (obj.name.toLowerCase().includes(token) ? 1 : 0);
      }
      return score;
    }
    for (let jsonObj of jsonArr) {
      let score = calcScore(jsonObj);
      if (score > 0) {
        results.push(jsonObj);
      }
    }
    return results;
  }

  render() {

    const { productList, filterMode } = this.props
    // const dataList = productList.filter(item => {
    //   //criteria to filter
    //   if (item.salePrice > this.state.filter.price) return item;
    // });

    const { isFilter } = filterMode
    let dataProduct = []
    if (isFilter != undefined && isFilter === true) {
      const { withFromTo, withDiscount, withCategory, dataFromTo, categoryKey, discount } = filterMode
      let datafromTo = []
      let dataDiscount = []
      let dataCategory = []
      let dataFilter = []
      let dataArray = []
      if (withFromTo === true) {
        datafromTo = productList.filter(item => {
          if (item.salePrice >= dataFromTo.from && item.salePrice <= dataFromTo.to) return item
        })
      }
      if (withDiscount === true) {
        dataDiscount = productList.filter(item => {
          let discountValue = (1 - item.salePrice / item.originalPrice) * 100
          if (discountValue >= discount) return item
        })
      }
      if (withCategory === true) {
        dataCategory = productList.filter((item) => {
          let name = this.getNameCategoryById(item.categoryId)
          if (name === categoryKey) return item
        })
      }

      if (withFromTo === true) {
        let arrayComp1 = [], arrayComp2 = []
        if (withDiscount === true) {
          arrayComp1 = datafromTo.filter((item) => {
            if (this.checkInclucesProduct(dataDiscount, item) === true) return item
          })
          if (withCategory === true) {
            arrayComp2 = arrayComp1.filter((item) => {
              let name = this.getNameCategoryById(item.categoryId)
              if (name === categoryKey) return item
            })
            dataProduct = arrayComp2
          } else {
            dataProduct = arrayComp1
          }
        } else {
          if (withCategory === true) {
            arrayComp1 = datafromTo.filter((item) => {
              let name = this.getNameCategoryById(item.categoryId)
              if (name === categoryKey) return item
            })
            dataProduct = arrayComp1
          } else {
            dataProduct = datafromTo
          }
        }
      } else {
        if (withDiscount === true) {
          if (withCategory === true) {
            let arrayComp3 = []
            arrayComp3 = dataDiscount.filter((item) => {
              let name = this.getNameCategoryById(item.categoryId)
              if (name === categoryKey) return item
            })
            dataProduct = arrayComp3
          }
          else {
            dataProduct = dataDiscount
          }
        } else {
          dataProduct = dataCategory
        }
      }

      // Cach 1
      // dataArray = [...datafromTo,...dataDiscount,...dataCategory]

      // for(let i = 0; i < dataArray.length; i++) {
      //    if(this.checkPushToProductList(dataFilter, dataArray[i]) === true) {
      //      dataFilter.push(dataArray[i])
      //    }
      // }
      //dataProduct = dataFilter

    } else {
      dataProduct = productList
    }

    let _dataProductOnSearch = []

    const { keySearch } = this.state

    if (keySearch != '') {
      _dataProductOnSearch = this.onSearch(dataProduct, keySearch)
    } else {
      _dataProductOnSearch = dataProduct
    }

    console.log('reRender')
    console.log(this.props)
    // console.log('Data Here ↓ ')
    // console.log(this.props.productList)
    return (
      <View style={{ flex: 1 }}>
        <View style={{ backgroundColor: 'red', flexDirection: 'row', paddingTop: (Platform.OS === 'ios') ? 10 : 20 }}>
          {
            Platform.OS === 'android' ?
              <SearchBar
                showLoading={false}
                platform="android"
                placeholder={i18n.t('placeholderSearch')}
                containerStyle={{ backgroundColor: 'red', width: width - 50 }}
                inputStyle={{ color: 'white' }}
                clearIcon={{ type: 'MaterialIcons', name: 'clear' }}
                rightIconContainerStyle={{ marginRight: 50 }}
                onChangeText={(Text) => {
                  this.setState({ keySearch: Text })
                }}
                onCancel={() => this.setState({ keySearch: '' })}
              /> :

              <SearchBar
                showLoading={false}
                platform="ios"
                cancelButtonTitle="Cancel"
                placeholder={i18n.t('placeholderSearch')}
                containerStyle={{ backgroundColor: 'red', width: width - 50 }}
                inputStyle={{ color: 'red' }}
                onChangeText={(Text) => {
                  this.setState({ keySearch: Text })
                }}
                onCancel={() => this.setState({ keySearch: '' })}
              //  rightIconContainerStyle={{ marginRight: 50 }}
              // cancelButtonProps={undefined}
              />
          }
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Icon
              color='white'
              name='shopping-cart'
              type='MaterialIcons'
              size={35}
              onPress={() => this.props.navigation.navigate('Cart')}
            />
          </View>
        </View>

        {/* <TouchableOpacity style={{  }}> */}
        <Icon
          raised
          name='filter'
          type='feather'
          color='red'
          size={20}
          onPress={this.switchToProductDetail}
          containerStyle={{ position: 'absolute', top: height / 5, zIndex: 10, left: 5 }}
        />
        {/* </TouchableOpacity> */}

        {
          (this.state.showModel === true) ? <FilterModal filterStatus={filterMode} /> : <View></View>
        }
        {
          (dataProduct.length === 0 || (keySearch != '' && _dataProductOnSearch.length === 0))
            ?
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ fontSize: 25, color: 'red' }}>{i18n.t('resultNotFoundTitle')}</Text>
            </View>
            :
            <ScrollView>
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                {
                  <FlatList
                    data={_dataProductOnSearch}
                    renderItem={this._renderItem}
                    keyExtractor={(item, index) => item.id}
                    style={{ flex: 1, width: width }}
                  />
                }
              </View>
            </ScrollView>
        }
      </View>
    );
  }
}

mapStateToProps = (state) => {
  return {
    productList: state.productRed.productList,
    filterMode: state.productRed.payloadFilter
  }
}



mapDispatchToProps = (dispatch) => {
  return {
    onCreate: () => { dispatch(ActionCreator.ACTION_CREATE) }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ProductScreen)









 //  title:i18n.t('tab2Name'),
      //  tabBarIcon: ({tintColor}) => (
      //   <Icon 
      //      name='list'
      //      type='Entypo'
      //      color='white'
      //   />
      // )