import React from 'react';
import { Text, View, FlatList, Dimensions, TouchableOpacity, Platform } from 'react-native';
import { connect } from 'react-redux'
import { Icon } from 'react-native-elements'
import ItemCartEdit from '../components/Cart/ItemCartEdit'
let { height, width } = Dimensions.get('screen')
import { Constants } from 'expo';
import i18n from '../i18n/i18n'

let sizeCart = 0
let itemPass = {}

class CartScreen extends React.Component {

    constructor(props) {
      super(props)
      const { cartList, navigation } = this.props
      
      if(cartList.length > 0 ) 
      sizeCart = cartList.length
      
      itemPass = cartList
    }

  onCheckoutEvent = () => {
    const { cartList, navigation } = this.props
    if(cartList.length != 0) {
      navigation.navigate('Checkout')
    }
    alert('Vo day roi ')
  }

  static navigationOptions = ({ navigation, tintColor }) => ({

    title: i18n.t('headerCart'),
    header: (
      <View style={{ height:50 + Constants.statusBarHeight ,backgroundColor: 'red', flexDirection: 'row', paddingTop:Constants.statusBarHeight}}>
         <TouchableOpacity style={{justifyContent:'center', alignItems:'center',backgroundColor:'red', flex:3, height: 50}}
                           onPress={()=> navigation.goBack()}
         >
            <Text style={{fontSize:18, color:'white'}}>{i18n.t('buttonBackTitle')}</Text>
         </TouchableOpacity>
         <View style={{flex: 7, height: 50, alignItems:'center', justifyContent:'center', backgroundColor:'red'}}>
            <Text style={{color:'white', fontSize:20, fontWeight:'bold'}}>{i18n.t('headerCart')}</Text>
         </View>
         <TouchableOpacity style={{justifyContent:'center', alignItems:'center' ,backgroundColor:'red', flex:3.5, height: 50}}
                           onPress={()=> (sizeCart > 0)? navigation.navigate('Checkout', { item: itemPass}): alert(i18n.t('messageCartEmpty'))}
         >
         <Text style={{fontSize:18, color:'white', marginRight:5 }}>Checkout</Text>
         </TouchableOpacity>
      </View>
    ),
  })

  _renderItem = ({item, index}) => {
     return <ItemCartEdit item={item} index={index}/>
  }

  render() {
    const { cartList } = this.props
    sizeCart =cartList.length
    itemPass = cartList
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop:10 }}>
      {
          (cartList.length != 0) ?
          <FlatList
          data={cartList}
          renderItem={this._renderItem}
          keyExtractor={(item, index) => index + ''}
          style={{ flex: 1, width: width }}
        />: <Text style={{fontSize:30, color:'red', fontWeight:'bold'}}>{i18n.t('messageCartEmpty')}</Text>
      }   
      </View>
    );
  }
}

mapStateToProps = (state) => {
  return {
    cartList: state.cartRed.cartList,
  }
}

export default connect(mapStateToProps)(CartScreen)