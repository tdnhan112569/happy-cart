import React from 'react';
import { Text, View, Dimensions, TouchableOpacity, ScrollView, Alert, Platform } from 'react-native';
import i18n from '../i18n/i18n'
import { Icon } from 'react-native-elements'
import ImageSlider from 'react-native-image-slider';
let { height, width } = Dimensions.get('screen')
import Category from '../API/fakeData/CategoryData'
import { connect } from 'react-redux'
import ActionCartCreator from '../redux/action/CartAction'
import {Constants} from 'expo'
class ProductDetailScreen extends React.Component {

  static navigationOptions = ({ navigation, tintColor }) => ({
    // headerTitle: navigation.getParam('title', 'Product Detail')
    headerTitle: 'Product Detail',
    header: (
      <View style={{ height: 50 + Constants.statusBarHeight, backgroundColor: 'red', flexDirection: 'row', paddingTop: Constants.statusBarHeight }}>
        <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: 'red', flex: 3, height:  50 }}
          onPress={() => navigation.goBack()}
        >
          <Text style={{ fontSize: 18, color: 'white' }}>{i18n.t('buttonBackTitle')}</Text>
        </TouchableOpacity>
        <View style={{ flex: 7, height: 50, alignItems: 'center', justifyContent: 'center', backgroundColor: 'red' }}>
          <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>{i18n.t('productDetailTitle')}</Text>
        </View>
        <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: 'red', flex: 3, height: 50 }}
          onPress={() => navigation.navigate('Cart')}
        >
          <Text style={{ fontSize: 18, color: 'white' }}>{i18n.t('cartButtonTitle')}</Text>
        </TouchableOpacity>
      </View >
    )
  })

  constructor(props) {
    super(props)
    const { navigation } = this.props;
    const item = navigation.getParam('item', {});
    this.state = {
      indexSize: 1,
      count: 1,
      totalPrice: (item != {} && item != undefined) ? item.salePrice : 0
    }
  }

  getCategoryByID = (id) => {
    const { body } = Category
    let object = body.filter((item) => {
      if (item.id === id) return item
    })
    return object[0].name
  }

  setColor = (index) => {
    if (this.state.indexSize === index) return 'red'
    return 'lightgray'
  }

  textChangeColor = (index) => {
    if (this.state.indexSize === index) return 'white'
    return 'black'
  }

  onSelectedSize = (index) => {
    this.setState({ indexSize: index })
  }

  onAddItem = () => {
    const { navigation } = this.props;
    const item = navigation.getParam('item', {});
    this.setState({ count: this.state.count + 1, totalPrice: this.state.totalPrice + item.salePrice })
  }

  onSubItem = () => {
    if (this.state.count > 1) {
      const { navigation } = this.props;
      const item = navigation.getParam('item', {});
      this.setState({ count: this.state.count - 1, totalPrice: this.state.totalPrice - item.salePrice })
    }
  }

  onPriceDisplay = (text) => {
    if ((text + '').length <= 3) return text + ''
    let i = 0
    let price = (text + '').split('')
    var priceToText = "";
    let count = 0
    for (i = price.length - 1; i >= 0; i--) {
      if (count === 3) {
        priceToText = price[i] + '.' + priceToText;
        count = 0;
      } else {
        priceToText = price[i] + priceToText
      }
      count++
    }
    return priceToText
  }

  onAddToCart = () => {
    const { navigation, onAddToCart } = this.props;

    console.log(this.props)

    const item = navigation.getParam('item', {});
    const { indexSize, count, totalPrice } = this.state

    if (item != {} && item != undefined) {

      let size = ''

      switch (indexSize) {
        case 0: size = 'S'; break
        case 1: size = 'M'; break
        case 2: size = 'L'; break
        case 3: size = 'XL'; break
        case 4: size = 'XXL'; break
        default:
          size = 'M'
      }

      let payload = {
        ...item,
        'totalPrice': totalPrice,
        'count': count,
        'status': 'Ordering',
        'size': size
      }

      console.log('Don hang dat ')
      console.log(payload)
      onAddToCart(payload)
      this.setState({ totalPrice: item.salePrice, count: 1 })
      Alert.alert(
        i18n.t('lableInformation'),
        i18n.t('lableResultBuyingSuccessful'),
        [
          { text: i18n.t('lableButtonGoToCart'), onPress: () => navigation.navigate('Cart'), style: 'cancel' },
          { text: i18n.t('lableBuyMore'), onPress: () => { console.log('Buy more') }},
        ],
        { cancelable: false }
      )

    }

  }


  render() {
    const { navigation } = this.props;
    const item = navigation.getParam('item', {});
    const { indexSize, count, totalPrice } = this.state

    console.log(item)
    return (
      <ScrollView style={{ maxHeight: height}}>
        <View style={{ flex: 1, alignItems: 'center', paddingBottom:(Platform.OS === 'ios')?20:0 }}>
          <View style={{ height: height * 0.5 }}>
            <ImageSlider
              loopBothSides
              autoPlayWithInterval={3000}
              images={item.images}
            />
          </View>
          <View style={{ height: height * 0.45, width: width }}>
            <View>
              <Text style={{ fontSize: 20, fontWeight: 'bold', marginLeft: 5, marginBottom: 5 }}>{i18n.t('lableDetailInformation')}</Text>
              <View style={{ flexDirection: 'row', height: 30, borderWidth: 1, borderColor: 'gray', marginLeft: 15, marginRight: 15 }}>
                <View style={{ flex: 3, backgroundColor: 'lightgray', justifyContent: 'center', paddingLeft: 10 }}>
                  <Text>{i18n.t('lableName')}</Text>
                </View>
                <View style={{ flex: 7, justifyContent: 'center', paddingLeft: 10 }}>
                  <Text >{item.name}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', height: 30, borderWidth: 1, borderColor: 'gray', marginLeft: 15, marginRight: 15 }}>
                <View style={{ flex: 3, backgroundColor: 'lightgray', justifyContent: 'center', paddingLeft: 10 }}>
                  <Text>{i18n.t('lableCategoryCheckout')}</Text>
                </View>
                <View style={{ flex: 7, justifyContent: 'center', paddingLeft: 10 }}>
                  <Text>{this.getCategoryByID(item.categoryId)}</Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', height: 30, borderWidth: 1, borderColor: 'gray', marginLeft: 15, marginRight: 15, marginBottom: 5 }}>
                <View style={{ flex: 3, backgroundColor: 'lightgray', justifyContent: 'center', paddingLeft: 10 }}>
                  <Text>{i18n.t('lableDescription')}</Text>
                </View>
                <View style={{ flex: 7, justifyContent: 'center', paddingLeft: 10 }}>
                  <Text>{item.shortDescription}</Text>
                </View>
              </View>
              <Text style={{ fontSize: 15, fontWeight: 'bold', marginLeft: 20, marginBottom: 5 }}>{i18n.t('lableChoiceSize')}</Text>
              <View style={{
                height: 50, width: width * 0.8, flexDirection: 'row', justifyContent: 'center',
                alignItems: 'center', marginLeft: 15, marginRight: 15, marginBottom: 5, alignSelf: 'center'
              }}>
                <View style={{ flex: 1, }}>
                  <TouchableOpacity style={{ height: 50, width: 50, borderRadius: 25, backgroundColor: this.setColor(0), alignItems: 'center', justifyContent: 'center' }}
                    onPress={() => this.onSelectedSize(0)}
                  >
                    <Text style={{ color: this.textChangeColor(0) }}>S</Text>
                  </TouchableOpacity>
                </View>
                <View style={{ flex: 1 }}>
                  <TouchableOpacity style={{ height: 50, width: 50, borderRadius: 25, backgroundColor: this.setColor(1), alignItems: 'center', justifyContent: 'center' }}
                    onPress={() => this.onSelectedSize(1)}
                  >
                    <Text style={{ color: this.textChangeColor(1) }}>M</Text>
                  </TouchableOpacity>
                </View>
                <View style={{ flex: 1 }}>
                  <TouchableOpacity style={{ height: 50, width: 50, borderRadius: 25, backgroundColor: this.setColor(2), alignItems: 'center', justifyContent: 'center' }}
                    onPress={() => this.onSelectedSize(2)}
                  >
                    <Text style={{ color: this.textChangeColor(2) }}>L</Text>
                  </TouchableOpacity>
                </View>
                <View style={{ flex: 1 }}>
                  <TouchableOpacity style={{ height: 50, width: 50, borderRadius: 25, backgroundColor: this.setColor(3), alignItems: 'center', justifyContent: 'center' }}
                    onPress={() => this.onSelectedSize(3)}
                  >
                    <Text style={{ color: this.textChangeColor(3) }}>XL</Text>
                  </TouchableOpacity>
                </View>
                <View style={{ flex: 1 }}>
                  <TouchableOpacity style={{ height: 50, width: 50, borderRadius: 25, backgroundColor: this.setColor(4), alignItems: 'center', justifyContent: 'center' }}
                    onPress={() => this.onSelectedSize(4)}
                  >
                    <Text style={{ color: this.textChangeColor(4) }}>XXL</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{
                width: width * 0.8, height: 50, marginLeft: 20, marginRight: 20, borderColor: 'red', marginTop: 10,
                borderWidth: 2, borderRadius: 10, alignSelf: 'center', flexDirection: 'row', alignItems: 'center'
              }}>
                <View style={{ flex: 5, flexDirection: 'row' }}>
                  <TouchableOpacity style={{
                    height: 30, width: 30, borderRadius: 15, alignItems: 'center',
                    justifyContent: 'center', borderWidth: 1, borderColor: 'red', marginLeft: 10
                  }}
                    onPress={this.onSubItem}
                  >
                    <Text style={{ color: 'red', fontSize: 20, fontWeight: 'bold' }}>-</Text>
                  </TouchableOpacity>
                  <Text style={{ fontSize: 20, flex: 3, textAlign: 'center' }}>{count}</Text>
                  <TouchableOpacity style={{ height: 30, width: 30, borderRadius: 15, alignItems: 'center', justifyContent: 'center', backgroundColor: 'red', marginRight: 10 }}
                    onPress={this.onAddItem}
                  >
                    <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>+</Text>
                  </TouchableOpacity>
                </View>
                <View style={{ flex: 5, alignItems: 'center', justifyContent: 'center', padding: 3 }}>
                  <TouchableOpacity style={{ flex: 1, width: width * 0.4, backgroundColor: 'red', alignItems: 'center', justifyContent: 'center', borderRadius: 10 }}
                    onPress={this.onAddToCart}
                  >
                    <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>{this.onPriceDisplay(totalPrice)}$</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}

mapDispatchToProps = (dispatch) => {
  return {
    onAddToCart: (payload) => {
      ActionCartCreator.ACTION_ADD_CART.payload = payload
      dispatch(ActionCartCreator.ACTION_ADD_CART)
    }
  }
}

export default connect(undefined, mapDispatchToProps)(ProductDetailScreen)