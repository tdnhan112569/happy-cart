/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, StatusBar } from 'react-native';
import { Provider } from 'react-redux'
import { createStore, combineReducers } from 'redux'
import TabNavigatorContainer from './navigation/ContainerTabNavigation'
import ProductReducer from './redux/reducer/ProductReducer'
import OrderReducer from './redux/reducer/OrderReducer'
import CartReducer from './redux/reducer/CartReducer'

const reducers = combineReducers({ productRed: ProductReducer, cartRed: CartReducer })
const store = createStore(reducers)

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        {/* <StatusBar hidden={true} /> */}
        <TabNavigatorContainer />
      </Provider>
    );
  }
}
