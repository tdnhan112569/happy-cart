let Category = {  
    "header":{  
       "status":200,
       "errorMessage":"",
       "currentDate":"2018-12-27T04:54:25.329Z",
       "totalCount":0,
    },
    "body":[  
       {  
          "id":"5b822e7f9c300309b7e9befc",
          "name":"men",
          "iconName":"men",
          "img":'https://oldnavy.gap.com/Asset_Archive/ONWeb/content/0009/426/201/assets/021715_US_EN_landingpage_vi_mtall.jpg'
       },
       {  
          "id":"5b822e919c300309b7e9bf0e",
          "name":"women",
          "iconName":"women",
          'img':'http://images.nyandcompany.com/is/image/NewYorkCompany/113018-Tall_Header.desktop?scl=1&qlt=90,1'
       },
       {  
          "id":"5b822ea99c300309b7e9bf13",
          "name":"accessories",
          "iconName":"accessories",
          'img':'http://coveteur.com/wp-content/uploads/2018/03/Spring_Market_Story-8-835x557.jpg'
       }
    ],
    "pagination":{  
       "limit":10,
       "skip":0,
       "total":3
    }
 }

 export default Category